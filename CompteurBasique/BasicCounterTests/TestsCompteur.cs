﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Application;

namespace BasicCounterTests
{
    [TestClass]
    public class TestsCompteur
    {
        [TestMethod]
        public void TestIncrementation()
        {
            BasicCounterClass valeur = new BasicCounterClass(2);


            valeur.Incrementation();

            Assert.AreEqual(3, valeur.getvaleurCompteur());
            Assert.AreNotEqual(1, valeur.getvaleurCompteur());
        }
                     
        

        [TestMethod]
        public void TestDecrementation()
        {
            BasicCounterClass valeur = new BasicCounterClass(2);

            valeur.Decrementation();

            Assert.AreEqual(0, valeur.getvaleurCompteur());

            BasicCounterClass valeur2 = new BasicCounterClass(2);


            valeur.Decrementation();
            Assert.AreEqual(0, valeur.getvaleurCompteur());
            Assert.AreNotEqual(199, valeur.getvaleurCompteur());

        }
        [TestMethod]
        public void TestRAZ()
        {
            BasicCounterClass valeur = new BasicCounterClass(2);


            valeur.RAZ();

            Assert.AreEqual(0, valeur.getvaleurCompteur());
            Assert.AreNotEqual(12, valeur.getvaleurCompteur());
        }
    }
}
