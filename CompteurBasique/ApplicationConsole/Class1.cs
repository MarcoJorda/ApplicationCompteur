﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Application
{
    public class BasicCounterClass
    {
        private int valeurCompteur;
            
        public BasicCounterClass(int valeurCompteur)
        {
            this.valeurCompteur = valeurCompteur;
        }

        public void Incrementation()
        {
           this.valeurCompteur = valeurCompteur+1;           
        }

        public void Decrementation()
        {
            if (this.valeurCompteur == 0)
            {

            }
            else
            {
                this.valeurCompteur = valeurCompteur - 1;
            }
        }

        public void RAZ()
        {
            this.valeurCompteur = 0;
        }
        public int getvaleurCompteur()
        {
            return (this.valeurCompteur);
        }
    }
}
