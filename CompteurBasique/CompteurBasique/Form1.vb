﻿Imports Application

Public Class BasicCounter
    Dim valeurCompteur As New BasicCounterClass(0)

    Private Sub BoutonIncrementation_Click(sender As Object, e As EventArgs) Handles BoutonIncrementation.Click
        valeurCompteur.Incrementation()
        LabelCompteur.Text = valeurCompteur.getvaleurCompteur()
    End Sub

    Private Sub BoutonDecrementation_Click(sender As Object, e As EventArgs) Handles BoutonDecrementation.Click
        valeurCompteur.Decrementation()
        LabelCompteur.Text = valeurCompteur.getvaleurCompteur()
    End Sub

    Private Sub BoutonRAZ_Click(sender As Object, e As EventArgs) Handles BoutonRAZ.Click
        valeurCompteur.RAZ()
        LabelCompteur.Text = valeurCompteur.getvaleurCompteur()
    End Sub


End Class
