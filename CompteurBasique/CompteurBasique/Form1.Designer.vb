﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BasicCounter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.LabelCompteur = New System.Windows.Forms.Label()
        Me.LabelTotal = New System.Windows.Forms.Label()
        Me.BoutonDecrementation = New System.Windows.Forms.Button()
        Me.BoutonIncrementation = New System.Windows.Forms.Button()
        Me.BoutonRAZ = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'LabelCompteur
        '
        Me.LabelCompteur.AutoSize = True
        Me.LabelCompteur.Font = New System.Drawing.Font("Microsoft Sans Serif", 25.0!)
        Me.LabelCompteur.Location = New System.Drawing.Point(386, 158)
        Me.LabelCompteur.Name = "LabelCompteur"
        Me.LabelCompteur.Size = New System.Drawing.Size(36, 39)
        Me.LabelCompteur.TabIndex = 0
        Me.LabelCompteur.Text = "0"
        '
        'LabelTotal
        '
        Me.LabelTotal.AutoSize = True
        Me.LabelTotal.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.LabelTotal.Location = New System.Drawing.Point(382, 85)
        Me.LabelTotal.Name = "LabelTotal"
        Me.LabelTotal.Size = New System.Drawing.Size(40, 17)
        Me.LabelTotal.TabIndex = 1
        Me.LabelTotal.Text = "Total"
        '
        'BoutonDecrementation
        '
        Me.BoutonDecrementation.Location = New System.Drawing.Point(117, 173)
        Me.BoutonDecrementation.Name = "BoutonDecrementation"
        Me.BoutonDecrementation.Size = New System.Drawing.Size(75, 23)
        Me.BoutonDecrementation.TabIndex = 2
        Me.BoutonDecrementation.Text = "-"
        Me.BoutonDecrementation.UseVisualStyleBackColor = True
        '
        'BoutonIncrementation
        '
        Me.BoutonIncrementation.Location = New System.Drawing.Point(631, 173)
        Me.BoutonIncrementation.Name = "BoutonIncrementation"
        Me.BoutonIncrementation.Size = New System.Drawing.Size(75, 23)
        Me.BoutonIncrementation.TabIndex = 3
        Me.BoutonIncrementation.Text = "+"
        Me.BoutonIncrementation.UseVisualStyleBackColor = True
        '
        'BoutonRAZ
        '
        Me.BoutonRAZ.Location = New System.Drawing.Point(366, 315)
        Me.BoutonRAZ.Name = "BoutonRAZ"
        Me.BoutonRAZ.Size = New System.Drawing.Size(75, 23)
        Me.BoutonRAZ.TabIndex = 4
        Me.BoutonRAZ.Text = "RAZ"
        Me.BoutonRAZ.UseVisualStyleBackColor = True
        '
        'BasicCounter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.BoutonRAZ)
        Me.Controls.Add(Me.BoutonIncrementation)
        Me.Controls.Add(Me.BoutonDecrementation)
        Me.Controls.Add(Me.LabelTotal)
        Me.Controls.Add(Me.LabelCompteur)
        Me.Name = "BasicCounter"
        Me.Text = "BasicCounter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents LabelCompteur As Label
    Friend WithEvents LabelTotal As Label
    Friend WithEvents BoutonDecrementation As Button
    Friend WithEvents BoutonIncrementation As Button
    Friend WithEvents BoutonRAZ As Button
End Class
